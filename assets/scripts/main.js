/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
	        $('.wpfp-link[title="Like"]').click(function(){
		        var $thumb = $(this).closest('.wpfp-span'),
		        	$likeCount = $thumb.next('span'),
		        	value = parseInt($likeCount.text(), 10) + 1;
		        $likeCount.text(value);
		        // $thumb.addClass('showThumb').addClass('liked');
			    setTimeout(function(){ $thumb.addClass('showThumb').addClass('liked'); }, 1500);
			});
	        $('.wpfp-link[title="Unlike"]').click(function(){
		        var $thumb = $(this).closest('.wpfp-span'),
		        	$likeCount = $thumb.next('span'),
		        	value = parseInt($likeCount.text(), 10) - 1;
		        $likeCount.text(value);
			    // $thumb.addClass('showThumb'); 
			    setTimeout(function(){ $thumb.addClass('showThumb'); }, 1500);
			});			
		        // https://jsfiddle.net/Narek_T/wsemLhtz/6/
		        var box, x;
				$('.handle').click(function() {
				  // var box = $(".box-inner"), x;
				  if ($(this).hasClass('handleNext')) {
					box = $(this).prev('div');
					boxElement = $(this).prev('div')[0];
				    x = ((box.width() / 2)) + box.scrollLeft();
				    box.animate({
				      scrollLeft: x,
				    });
				    $(this).siblings('.handlePrev').addClass('show');
				    if ( x >= ( boxElement.scrollWidth - box.width() ) ) {
						$(this).removeClass('show');    
				    }
					// console.log( x );
				    // console.log( boxElement.scrollWidth );
				  } else {
					box = $(this).next('div');
				    x = ((box.width() / 2)) - box.scrollLeft();
				    box.animate({
				      scrollLeft: -x,
				    });
				    $(this).siblings('.handleNext').addClass('show');
					if ( x >= 0 ) {    
						$(this).removeClass('show');    
				    }
				    // console.log( x );
				  }
				});			
		
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
        
        		
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
