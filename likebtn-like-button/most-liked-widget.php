<?php
/**
 * Most Liked Content tempplate
 */

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
?>

<?php if (!empty($before_widget)): ?>
    <?php echo $before_widget; ?>
<?php endif ?>

<?php if (!empty($title)): ?>
    <?php if (!empty($before_title)): ?>
        <?php echo $before_title; ?>
    <?php endif ?>
	<?php echo $title; ?>
    <?php if (!empty($after_title)): ?>
        <?php echo $after_title; ?>
    <?php endif ?>
<?php endif ?>

<?php if (count($post_loop) > 0): ?>
	<div class="likebtn-mlw">
	<?php foreach ($post_loop as $post): ?>
		<div id="post-<?php echo $post['id'] ?>" class="likebtn-mlw-item um um-item uimob960" >
			<div class="zr-item-poster">
	            <a href="<?php echo $post['link'] ?>?forsale" title="<?php echo $post['title'] ?>">
	                <?php if ($show_thumbnail): ?>
	                    <?php echo wp_get_attachment_image( get_field('cover_art', $post['id']), 'poster-sm', false, 'class=alignleft' ); ?>
	                <?php endif ?>
	            </a>
				<a href="<?php echo $post['link'] ?>?forsale" title="<?php echo $post['title'] ?>">
				<div class="content-overlay">
				    <a class="play-icon-wrap" href="<?php echo $post['link'] ?>?forsale">
				    <div class="play-icon-wrap-relOFF">
				        <div class="play-icon-wrap-rel-ring"></div><span class="play-icon-wrap-rel-play"><i aria-hidden="true" class="fa fa-play fa-2x"></i></span>
				    </div></a>
				</div>
				</a>
			</div>


			<div class="zr-item-meta">
				<div class="likebtn-mlw-title um-item-link">
				    <a href="<?php echo $post['link'] ?>?forsale" title="<?php echo $post['title'] ?>">
				            <h3><?php echo $post['title'] ?></h3>
<?php if (1 ==2): // DONT SHOW ?>
				            <?php if ($show_likes || $show_dislikes): ?>&nbsp;<span class="likebtn-item-likes"><nobr>(
				            <?php endif ?>
				            <?php echo $show_likes ? $post['likes'] : ''; ?>
				            <?php if ($show_likes && $show_dislikes): ?>
				                /
				            <?php endif ?>
				            <?php echo $show_dislikes ? $post['dislikes'] : ''; ?>
				            <?php if ($show_likes || $show_dislikes): ?>
				                )</nobr></span>
				            <?php endif ?>
<?php endif ?>
				    </a>
			    </div>
			    <div class="zr-item-desc">
					<span class="published"><?php echo sprintf(__('%s ago','ultimate-member'), human_time_diff( get_the_time('U', $post['id']), current_time('timestamp') ) ); ?></span>
					<span class="published likebtn-mlw-date"><i><?php echo date_i18n(get_option('date_format'), $post['date']) ?></i></span>

		            <?php // echo '<p>' . wp_trim_words( get_field('video_description' ), $num_words = 18, $more = '...' ) . '</p>'; ?>
					<?php
					$long_text = get_field('video_description', $post['id'] );

					$max_length = wp_is_mobile() ? 60 : 97; // we want to show only 40 characters.

					if (strlen($long_text) > $max_length)
					{
						$short_text = (substr($long_text,0,$max_length-1)); // make it $max_length chars long
						$short_text .= "..."; // add an ellipses ... at the end
						// $short_text .= "<a href='#'>Read more</a>"; // add a link
						echo '<p>' . $short_text . '</p>';
					} else {
						// string is already less than $max_length, so display the string as is
						echo '<p>' . $long_text . '</p>';
					}
					// if (isset($showmetrics)) {
					//	get_template_part('partials/video', 'metrics');
					// }
?>
<!-- partials/video-metrics -->
<section class="metrics">
	<?php echo do_shortcode('[post-views id="' . $post['id'] . '"]'); ?>
	<?php echo do_shortcode( '[likebtn identifier="post_' . $post['id'] . '" theme="custom" btn_size="22" f_size="16" icon_size="22" icon_l_c_v="#e0c264" icon_d_c_v="#e0c264" counter_l_c="#ffffff" bg_c="rgba(250,250,250,0)" brdr_c="rgba(198,198,198,0)" like_enabled="0" dislike_enabled="0" show_like_label="0" icon_dislike_show="0" tooltip_enabled="0" white_label="1" rich_snippet="1" voter_by="user" user_logged_in="1" popup_disabled="1" popup_style="dark" share_enabled="0"]' ); ?><?php // echo $post['likes']; ?>
	<a class="count-comments" href="<?php the_permalink(); ?>#comment"><?php echo get_comments_number($post['id']); ?></a>
</section>
			    </div>
<!--
			    <?php // if ($show_date && $post['date']): ?>
			        <small class="likebtn-mlw-date"><i><?php // echo date_i18n(get_option('date_format'), $post['date']) ?></i></small>
			    <?php // endif ?>
			    <?php // if ($show_author && $post['author_name']): ?>
			        <?// php if ($show_date && $post['date']): ?>
			            <small>/</small>
			        <?// php endif ?>
			        <small class="likebtn-mlw-author"><i><?php // echo $post['author_name'] ?></i></small>
			    <?php // endif ?>
			    <?php // if ($show_excerpt): ?>
			        <div class="likebtn-mlw-excerpt"><?php // echo $post['excerpt'] ?></div>
			    <?php // endif ?>
			    <?php // if ($post['button_html']): ?>
			        <div class="likebtn-mlw-button"><?php // echo $post['button_html']; ?></div>
			    <?php // endif ?>
			    <?php // if ($show_thumbnail || $show_excerpt): ?>
			        <br/>
			    <?php // endif ?>
-->
			</div>
		</div>
	<?php endforeach; ?>
	</div>
<?php else: // No items ?>
	<div class="likebtn-mlw-no-items">
		<p><?php _e('No items liked yet.', LIKEBTN_I18N_DOMAIN); ?></p>
	</div>
<?php
endif;
?>

<?php if (!empty($after_widget)): ?>
    <?php echo $after_widget; ?>
<?php endif ?>