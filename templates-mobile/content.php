<!-- templates-mobile/content -->
<?php
	// 220 x 316 = pad-bottom 143.6 x 48%
	
	$file = get_field('video');
	if( $file ): ?>

<article class="tile">
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
	<div class="tile__media">
		<img class="tile__img" src="<?php the_post_thumbnail_url('poster-vertical-sm'); ?>">
	</div>
	<div class="tile__details">
		<div class="tile__title">
			<?php // the_title(); ?>
  		</div>
	</div>	
	</a>
<?php // get_template_part('partials/video', 'metrics'); ?>
</article>

<?php endif; ?>