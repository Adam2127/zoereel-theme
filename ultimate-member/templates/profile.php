<div class="um <?php echo $this->get_class( $mode ); ?> um-<?php echo $form_id; ?> um-role-<?php echo um_user('role'); ?> ">

	<div class="um-form">

		<?php do_action('um_profile_before_header', $args ); ?>

		<?php if ( um_is_on_edit_profile() ) { ?><form method="post" action=""><?php } ?>

			<?php // do_action('um_profile_header_cover_area', $args ); ?>

			<?php do_action('um_profile_header', $args ); ?>
<?php // echo '<pre>'; print_r ($args); echo '</pre>'; ?>
			<?php do_action('um_profile_navbar', $args ); ?>

			<?php

			$nav = $ultimatemember->profile->active_tab;
			$subnav = ( get_query_var('subnav') ) ? get_query_var('subnav') : 'default';

			print "<div class='um-profile-body $nav $nav-$subnav'>";

				// Custom hook to display tabbed content
				do_action("um_profile_content_{$nav}", $args);
				do_action("um_profile_content_{$nav}_{$subnav}", $args);

			print "</div>";

// Show user Bookmark/Favorites
// echo do_shortcode('[cbxwpbookmark userid="'.um_user("ID").'"]');
			?>

		<?php if ( um_is_on_edit_profile() ) { ?></form><?php } ?>

	</div>

</div>

<script>
	jQuery( window ).load(function() {
	  jQuery('.um-profile-edit-a').click(function(){
		  jQuery('body').addClass('um-dropdown-hide');
	  })
	});

  document.addEventListener("DOMContentLoaded", function(event) {
	var elmnt = document.getElementsByClassName("active");
	if (!elmnt[1].classList.contains('um-profile-nav-posts')) {
    	elmnt[1].scrollIntoView(false, {behavior: "instant", block: "center", inline: "nearest"});
    }
  });

</script>