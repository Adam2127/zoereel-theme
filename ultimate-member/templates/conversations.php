 <?php
// echo 'seseeeme user: ' . $user;
// var_dump($user);
$i = 0; if ( $conversations ) {

?>

<div class="um um-viewing">
	<div class="um-message-conv">

		<?php

		foreach( $conversations as $conversation ) {

			if ( $conversation->user_a == um_profile_id() ) {
				$user = $conversation->user_b;
			} else {
				$user = $conversation->user_a;
			}

			if ( $um_messaging->api->blocked_user( $user ) ) continue;
			if ( $um_messaging->api->hidden_conversation( $conversation->conversation_id ) ) continue;

			$i++;

			if ( $i == 1 && !isset( $current_conversation ) ) {
				$current_conversation = $conversation->conversation_id;
			}

			um_fetch_user( $user );

			$user_name = ( um_user('display_name') ) ? um_user('display_name') : __('Deleted User','um-messaging');

			$is_unread = $um_messaging->api->unread_conversation( $conversation->conversation_id, um_profile_id() );

		?>

		<a href="<?php echo add_query_arg('conversation_id', $conversation->conversation_id ); ?>" class="um-message-conv-item <?php if ( $conversation->conversation_id == $current_conversation ) echo 'active '; ?>" data-message_to="<?php echo $user; ?>" data-trigger_modal="conversation" data-conversation_id="<?php echo $conversation->conversation_id; ?>" data-object_id="<?php echo $conversation->object_id; ?>">

			<span class="um-message-conv-name"><?php echo $user_name; ?></span>

			<span class="um-message-conv-pic"><?php echo get_avatar( $user, 40 ); ?></span>

			<?php if ( $is_unread ) { ?><span class="um-message-conv-new"><i class="um-faicon-circle"></i></span><?php } ?>

			<?php do_action('um_messaging_conversation_list_name'); ?>

		</a>

		<?php } ?>

	</div>

	<div class="um-message-conv-view">

		<?php
		$i = 0;
		foreach( $conversations as $conversation ) {

				if ( isset( $current_conversation ) && $current_conversation != $conversation->conversation_id )
					continue;

				if ( $conversation->user_a == um_profile_id() ) {
					$user = $conversation->user_b;
				} else {
					$user = $conversation->user_a;
				}

				if ( $um_messaging->api->blocked_user( $user ) ) continue;
				if ( $um_messaging->api->hidden_conversation( $conversation->conversation_id ) ) continue;

				$i++; if ( $i > 1 ) continue;

				um_fetch_user( $user );

				$user_name = ( um_user('display_name') ) ? um_user('display_name') : __('Deleted User','um-messaging');

				$um_messaging->api->conversation_template( $user, $user_id, get_queried_object_id() );

		}
	?>

	</div><div class="um-clear"></div>
</div>

<?php } else { ?>

<!--
<div class="um-message-noconv">
	<i class="um-icon-android-chat"></i>
	<?php // _e('No chats found here','um-notifications'); ?>
</div>
-->
<div class="um um-viewing uimob960" style="opacity: 1;">
    <div class="um-message-conv-view">
        <div class="um-message-header um-popup-header">
            <div class="um-message-header-left">
                <img alt="" class="func-um_user gravatar avatar avatar-40 um-avatar um-avatar-uploaded" height="40" src="/app/uploads/ultimatemember/9/profile_photo.jpg" width="40"><a class="fn" href="https://zoereel.dev/author/hoang-ha/" rel="author"></a>
            </div>
        </div>
        <div class="um-message-body um-popup-autogrow um-message-autoheight" data-message_to="">
            <div class="um-message-ajax" data-conversation_id="" data-last_updated="" data-message_to=""></div>
        </div>
        <div class="um-message-footer um-popup-footer" data-limit_hit="You have reached your limit for sending messages.">
            <div class="um-message-textarea">
                <div class="um-message-emoji">
                    <a class="um-message-emo" href="#"><img alt="" src="https://zoereel.dev/app/plugins/um-messaging/assets/img/emoji_init.png" title=""></a> <span class="um-message-emolist"><span class="um-message-insert-emo" data-emo=":)" title=":)"><img alt=":)" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f604.png" title=":)"></span> <span class="um-message-insert-emo" data-emo=":smiley:" title=":smiley:"><img alt=":smiley:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f603.png" title=":smiley:"></span> <span class="um-message-insert-emo" data-emo=":D" title=":D"><img alt=":D" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f600.png" title=":D"></span> <span class="um-message-insert-emo" data-emo=":$" title=":$"><img alt=":$" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f60a.png" title=":$"></span> <span class="um-message-insert-emo" data-emo=":relaxed:" title=":relaxed:"><img alt=":relaxed:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/263a.png" title=":relaxed:"></span> <span class="um-message-insert-emo" data-emo=";)" title=";)"><img alt=";)" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f609.png" title=";)"></span> <span class="um-message-insert-emo" data-emo=":heart_eyes:" title=":heart_eyes:"><img alt=":heart_eyes:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f60d.png" title=":heart_eyes:"></span> <span class="um-message-insert-emo" data-emo=":kissing_heart:" title=":kissing_heart:"><img alt=":kissing_heart:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f618.png" title=":kissing_heart:"></span> <span class="um-message-insert-emo" data-emo=":kissing_closed_eyes:" title=":kissing_closed_eyes:"><img alt=":kissing_closed_eyes:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f61a.png" title=":kissing_closed_eyes:"></span> <span class="um-message-insert-emo" data-emo=":kissing:" title=":kissing:"><img alt=":kissing:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f617.png" title=":kissing:"></span> <span class="um-message-insert-emo" data-emo=":kissing_smiling_eyes:" title=":kissing_smiling_eyes:"><img alt=":kissing_smiling_eyes:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f619.png" title=":kissing_smiling_eyes:"></span> <span class="um-message-insert-emo" data-emo=";P" title=";P"><img alt=";P" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f61c.png" title=";P"></span> <span class="um-message-insert-emo" data-emo=":P" title=":P"><img alt=":P" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f61b.png" title=":P"></span> <span class="um-message-insert-emo" data-emo=":stuck_out_tongue_closed_eyes:" title=":stuck_out_tongue_closed_eyes:"><img alt=":stuck_out_tongue_closed_eyes:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f61d.png" title=":stuck_out_tongue_closed_eyes:"></span> <span class="um-message-insert-emo" data-emo=":flushed:" title=":flushed:"><img alt=":flushed:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f633.png" title=":flushed:"></span> <span class="um-message-insert-emo" data-emo=":grin:" title=":grin:"><img alt=":grin:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f601.png" title=":grin:"></span> <span class="um-message-insert-emo" data-emo=":pensive:" title=":pensive:"><img alt=":pensive:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f614.png" title=":pensive:"></span> <span class="um-message-insert-emo" data-emo=":relieved:" title=":relieved:"><img alt=":relieved:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f60c.png" title=":relieved:"></span> <span class="um-message-insert-emo" data-emo=":unamused" title=":unamused"><img alt=":unamused" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f612.png" title=":unamused"></span> <span class="um-message-insert-emo" data-emo=":(" title=":("><img alt=":(" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f61e.png" title=":("></span> <span class="um-message-insert-emo" data-emo=":persevere:" title=":persevere:"><img alt=":persevere:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f623.png" title=":persevere:"></span> <span class="um-message-insert-emo" data-emo=":'(" title=":'("><img alt=":'(" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f622.png" title=":'("></span> <span class="um-message-insert-emo" data-emo=":joy:" title=":joy:"><img alt=":joy:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f602.png" title=":joy:"></span> <span class="um-message-insert-emo" data-emo=":sob:" title=":sob:"><img alt=":sob:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f62d.png" title=":sob:"></span> <span class="um-message-insert-emo" data-emo=":sleepy:" title=":sleepy:"><img alt=":sleepy:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f62a.png" title=":sleepy:"></span> <span class="um-message-insert-emo" data-emo=":disappointed_relieved:" title=":disappointed_relieved:"><img alt=":disappointed_relieved:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f625.png" title=":disappointed_relieved:"></span> <span class="um-message-insert-emo" data-emo=":cold_sweat:" title=":cold_sweat:"><img alt=":cold_sweat:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f630.png" title=":cold_sweat:"></span> <span class="um-message-insert-emo" data-emo=":sweat_smile:" title=":sweat_smile:"><img alt=":sweat_smile:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f605.png" title=":sweat_smile:"></span> <span class="um-message-insert-emo" data-emo=":sweat:" title=":sweat:"><img alt=":sweat:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f613.png" title=":sweat:"></span> <span class="um-message-insert-emo" data-emo=":weary:" title=":weary:"><img alt=":weary:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f629.png" title=":weary:"></span> <span class="um-message-insert-emo" data-emo=":tired_face:" title=":tired_face:"><img alt=":tired_face:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f62b.png" title=":tired_face:"></span> <span class="um-message-insert-emo" data-emo=":fearful:" title=":fearful:"><img alt=":fearful:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f628.png" title=":fearful:"></span> <span class="um-message-insert-emo" data-emo=":scream:" title=":scream:"><img alt=":scream:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f631.png" title=":scream:"></span> <span class="um-message-insert-emo" data-emo=":angry:" title=":angry:"><img alt=":angry:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f620.png" title=":angry:"></span> <span class="um-message-insert-emo" data-emo=":rage:" title=":rage:"><img alt=":rage:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f621.png" title=":rage:"></span> <span class="um-message-insert-emo" data-emo=":triumph" title=":triumph"><img alt=":triumph" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f624.png" title=":triumph"></span> <span class="um-message-insert-emo" data-emo=":confounded:" title=":confounded:"><img alt=":confounded:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f616.png" title=":confounded:"></span> <span class="um-message-insert-emo" data-emo=":laughing:" title=":laughing:"><img alt=":laughing:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f606.png" title=":laughing:"></span> <span class="um-message-insert-emo" data-emo=":yum:" title=":yum:"><img alt=":yum:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f60b.png" title=":yum:"></span> <span class="um-message-insert-emo" data-emo=":mask:" title=":mask:"><img alt=":mask:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f637.png" title=":mask:"></span> <span class="um-message-insert-emo" data-emo=":cool:" title=":cool:"><img alt=":cool:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f60e.png" title=":cool:"></span> <span class="um-message-insert-emo" data-emo=":sleeping:" title=":sleeping:"><img alt=":sleeping:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f634.png" title=":sleeping:"></span> <span class="um-message-insert-emo" data-emo=":dizzy_face:" title=":dizzy_face:"><img alt=":dizzy_face:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f635.png" title=":dizzy_face:"></span> <span class="um-message-insert-emo" data-emo=":astonished:" title=":astonished:"><img alt=":astonished:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f632.png" title=":astonished:"></span> <span class="um-message-insert-emo" data-emo=":worried:" title=":worried:"><img alt=":worried:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f61f.png" title=":worried:"></span> <span class="um-message-insert-emo" data-emo=":frowning:" title=":frowning:"><img alt=":frowning:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f626.png" title=":frowning:"></span> <span class="um-message-insert-emo" data-emo=":anguished:" title=":anguished:"><img alt=":anguished:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f627.png" title=":anguished:"></span> <span class="um-message-insert-emo" data-emo=":smiling_imp:" title=":smiling_imp:"><img alt=":smiling_imp:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f608.png" title=":smiling_imp:"></span> <span class="um-message-insert-emo" data-emo=":imp:" title=":imp:"><img alt=":imp:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f47f.png" title=":imp:"></span> <span class="um-message-insert-emo" data-emo=":open_mouth:" title=":open_mouth:"><img alt=":open_mouth:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f62e.png" title=":open_mouth:"></span> <span class="um-message-insert-emo" data-emo=":grimacing:" title=":grimacing:"><img alt=":grimacing:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f62c.png" title=":grimacing:"></span> <span class="um-message-insert-emo" data-emo=":neutral_face:" title=":neutral_face:"><img alt=":neutral_face:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f610.png" title=":neutral_face:"></span> <span class="um-message-insert-emo" data-emo=":confused:" title=":confused:"><img alt=":confused:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f615.png" title=":confused:"></span> <span class="um-message-insert-emo" data-emo=":hushed:" title=":hushed:"><img alt=":hushed:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f62f.png" title=":hushed:"></span> <span class="um-message-insert-emo" data-emo=":no_mouth:" title=":no_mouth:"><img alt=":no_mouth:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f636.png" title=":no_mouth:"></span> <span class="um-message-insert-emo" data-emo=":innocent:" title=":innocent:"><img alt=":innocent:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f607.png" title=":innocent:"></span> <span class="um-message-insert-emo" data-emo=":smirk:" title=":smirk:"><img alt=":smirk:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f60f.png" title=":smirk:"></span> <span class="um-message-insert-emo" data-emo=":expressionless:" title=":expressionless:"><img alt=":expressionless:" class="emoji" src="https://s.w.org/images/core/emoji/72x72/1f611.png" title=":expressionless:"></span></span>
                </div>
                <textarea data-maxchar="10000" id="um_message_text" name="um_message_text" placeholder="Type your message..."></textarea>
            </div>
            <div class="um-message-buttons">
                <span class="um-message-limit">10000</span> <a class="um-message-send disabled" href="#"><i class="um-faicon-envelope-o"></i>Send message</a>
            </div>
            <div class="um-clear"></div>
        </div>
    </div>
    <div class="um-clear"></div>
</div>

<?php } ?>
