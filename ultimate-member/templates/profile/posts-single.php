	<?php while ($ultimatemember->shortcodes->loop->have_posts()) { $ultimatemember->shortcodes->loop->the_post(); $post_id = get_the_ID(); ?>
		<?php if (get_field('for_sale') == 1 ) continue; ?>
		<div class="um-item">
			
			<div class="zr-item-poster">
				<a href="<?php the_permalink(); ?>"><?php echo wp_get_attachment_image( get_field('cover_art'), 'medium' ); ?></a>
				<div class="content-overlay">
					<a class="play-icon-wrap" href="<?php the_permalink(); ?>">
			    		<div class="play-icon-wrap-relOFF">
			    	 		<div class="play-icon-wrap-rel-ring"></div>
					 		<span class="play-icon-wrap-rel-play">
					 			<i class="fa fa-play fa-2x" aria-hidden="true"></i>
					 		</span>
			    	 	</div>
			    	</a>
				</div>
			</div>
			
<?php 
	$long_text = get_field('video_description' );
				
	$num_words = wp_is_mobile() ? 12 : 18; // we want to show only 40 characters.
	
// 	if (strlen($long_text) > $max_length)
// 	{
// 		$short_text = (substr($long_text,0,$max_length-1)); // make it $max_length chars long
// 		$short_text .= "..."; // add an ellipses ... at the end
// 		// $short_text .= "<a href='#'>Read more</a>"; // add a link
// 		echo '<p>' . $short_text . '</p>';
// 	} else {
// 		// string is already less than $max_length, so display the string as is
// 		echo '<p>' . $long_text . '</p>';
// 	}
?>			
			<div class="zr-item-meta">
				<div class="um-item-link"><a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a></div>
				<span class="published"><?php echo sprintf(__('%s ago','ultimate-member'), human_time_diff( get_the_time('U'), current_time('timestamp') ) ); ?></span>
<!-- 				<span><?php // echo __('in','ultimate-member');?>: <?php // the_category( ', ' ); ?></span> -->
<!-- 				<span><?php // comments_number( __('no comments','ultimate-member'), __('1 comment','ultimate-member'), __('% comments','ultimate-member') ); ?></span> -->
<!-- 				<div class="zr-item-desc"><?php // the_content(); ?></div> -->
				<div class="zr-item-desc"><?php echo wp_trim_words( get_field('video_description' ), $num_words, $more = '...' ); ?></div>
<!-- 				<div class="zr-item-desc"><?php // the_field('video_description'); ?></div> -->
				<?php get_template_part('partials/video', 'metrics'); ?></div>
				<div class="editVidLink um-profile-nav-item active">
					<a href="<?php the_permalink(); ?>/?edit" title="Videos">
						<i class="um-faicon-pencil"></i>
						<span class="uimob500-hide uimob340-hide uimob800-hide title">Edit</span>
					</a>
				</div>
		</div>
		
	<?php } ?>
	
	<?php if ( isset($ultimatemember->shortcodes->modified_args) && $ultimatemember->shortcodes->loop->have_posts() && $ultimatemember->shortcodes->loop->found_posts >= 10 ) { ?>
	
		<div class="um-load-items">
			<a href="#" class="um-ajax-paginate um-button" data-hook="um_load_posts" data-args="<?php echo $ultimatemember->shortcodes->modified_args; ?>"><?php _e('load more posts','ultimate-member'); ?></a>
		</div>
		
	<?php } ?>