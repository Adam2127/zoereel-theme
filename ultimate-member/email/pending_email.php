Hi {display_name},

Thank you for signing up with {site_name}! Your account is currently being reviewed by a member of our team.

Zoereel, where New Ideas in TV & Film Live. with your participation in Zoereel, together we aim for better movies, TV shows, new filmmakers, and news stories.

Please allow us some time to process your request.

We will email you once your account is activated.
If you have any questions , please contact us at info@zoereel.com


Thanks,
{site_name} Team.