Hi {display_name},

You've received a new {rating} review from {reviewer}!

Here is the review content:

{review_content}

{reviews_link}

This is an automated notification from {site_name}. You do not need to reply.