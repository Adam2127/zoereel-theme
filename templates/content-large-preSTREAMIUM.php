<!-- templates/content-large -->
<?php
	$file = get_field('video');
	if( $file ): ?>

<article class="tile">
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
	<div class="tile__media">
<!-- 		<video class="tile__img" src="<?php // echo $file['url']; ?>" poster="<?php // the_post_thumbnail_url(); ?>"><?php // echo $file['filename']; ?></video> -->

			<?php the_post_thumbnail('poster-big', ['class' => 'featured', 'title' => 'Feature Video']); ?>
	</div>
	<div class="tile__details">
		<div class="tile__title">
			<?php the_title(); ?>
  		</div>
	</div>	
	</a>
</article>

<?php endif; ?>