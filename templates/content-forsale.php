<?php acf_form_head(); ?>
<?php
	$userVideo = ( ($post->post_author == get_current_user_id() ) ? true : false );
	if( $userVideo ) {
if (is_super_admin()) { echo '<!--  ' . basename(__FILE__) . ' -->'; }	
if (is_super_admin()) { echo '<!--  seee acf_form_head -->'; }	
	    acf_form_head();
if (is_super_admin()) { echo '<!--  end seee acf_form_head -->'; }		    
	}
?>

<!-- templates/content-single -->
<div id="back" onclick="goBack()">
    <span></span>
    <span></span>
</div>

<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class('for-sale'); ?>>
<?php 
	$file   = get_field('video');
 	$poster = get_the_post_thumbnail_url( '','poster-big' ); 	
 	$price  = get_field( 'movie_price' );
 	$current_user = wp_get_current_user();
	?>	
	<div style="width: 1080px;" class="wp-video">
		<video id="video-<?php echo $post->ID;?>" poster="<?php echo $poster; ?>" playsinline preload="auto">
			<source type="video/mp4" src="<?php echo $file['url']; ?>" />
			<a href="<?php echo $file['url']; ?>"><?php echo $file['url']; ?></a>
		</video>
	</div>

    <header>
      <h2 class="entry-title"><?php the_title(); ?></h2>
        <div class="entry-content noshare">
			<?php // the_content(); ?>
			<p><?php the_field('video_description'); ?></p>
			<?php get_template_part('partials/entry', 'taxonomy'); ?>
    	</div>
      <?php $showcredits = true; include( locate_template( 'partials/entry-meta.php', false, false ) ); ?>
        <div id="moviePoster">
		  <img src="<?php echo $poster; ?>">
		</div>
    </header>   
    
    <section id="pitch">
	    <br>
	    <h5><?= __('Elevator Pitch: ', 'zoereel'); ?></h5>
	    <p class="entry-content"><?php the_field('the_pitch'); ?></p>	
    </section> 
	<?php 
		$authorID = get_the_author_meta('ID');
	?>	        
    <section id="objectID" class="<?php echo ($userVideo) ? 'is-artist': 'not-artist'; ?>" data-artist="<?php echo $authorID ?>" data-zoereel_vid="<?php echo $post->ID;?>" data-object_id="<?php echo $post->ID;?>">
	    <br>
	    <?php if( $price ){
		    echo '<h3>Price: $' . number_format( $price ) . '</h3>';
		    echo '<button id="openmessage" class="um-button"><span>Strike a Deal &amp; Contact Seller</span></button>';
		    echo '<button id="artistmessage" class="um-button"><span>A Buyer is Interested: Strike a Deal</span></button>';
	    } ?>
	    <div id="dealbutton">
	    <?php 
// 		    $shortcodebutton = sprintf(
// 			    '[ultimatemember_message_button user_id="%1$s"]',$authorID
// 			);
		    // echo do_shortcode($shortcodebutton);
		    
//		    $userID = ($userVideo) ? 0 : get_current_user_id(); 
		    // get a unique number from the user ID and post ID
		    // https://stackoverflow.com/questions/919612/mapping-two-integers-to-one-in-a-unique-and-deterministic-way a >= b ? a * a + a + b : a + b * b;
//   // $UserPostID = ($userID >= $post->ID) ? $userID * $userID + $userID + $post->ID : $userID + $post->ID * $post->ID;
//   $ArtistPostID = ($authorID >= $post->ID) ? $authorID * $authorID + $authorID + $post->ID : $authorID + $post->ID * $post->ID;
//   // 	
//   echo 'current user ' . $userID;
//   // echo '<br>post id ' . $post->ID;
//   // echo '<br>unquie number user & post ' . $UserPostID;
//   echo '<br>current artist ' . $authorID;
//   echo '<br>unquie number artist & post ' . $ArtistPostID;	
    
		    $shortcodepanel = sprintf(
			    '[ultimatemember_messages user_id="%1$s", content_id="%2$s"]',get_current_user_id(), $post->ID
			);
		    echo do_shortcode($shortcodepanel);
		?>
	    <?php // acf_form('strike_deal'); ?>
	    </div>
    </section>      
    
    <script>
		// document.getElementById("acf-_post_title").value = 'Deal for: <?php the_title(); ?> by <?php echo $current_user->display_name; ?> on <?php echo date("Y-m-d") ?>';
		// document.getElementById("acf-field_597bbb3062fcc").value = '<?php echo $price; ?>';
	</script>
    

	<script>
	function goBack() {
		    window.history.back();
		    console.log(document.referrer);
		}
	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}		
	jQuery(window).load(function(){
		// jQuery('#dealbutton [data-message_to=<?php echo $authorID ?>]').click();
		// jQuery('#objectID').data('artist');
		<?php if( !$userVideo ) { ?>
			var artistID = jQuery('#objectID').data('artist'),
				objectID = jQuery('#objectID').data('object_id');
			if ( jQuery('#dealbutton [data-message_to=' + artistID + ']').length ) {
				// jQuery('#dealbutton [data-message_to=' + artistID + ']').trigger( 'click' ).addClass('seeesome');
				jQuery('#dealbutton [data-object_id=' + objectID + ']').trigger( 'click' ).addClass('seeesome');
				jQuery('#dealbutton').addClass('messages');
			} else {
				jQuery('#dealbutton').addClass('no-messages');
				jQuery('#openmessage').addClass('show');
				jQuery('[data-message_to]').attr('data-message_to', artistID);
				var headerstring = '<img src="/app/uploads/ultimatemember/'+artistID+'/profile_photo.jpg" class="func-um_user gravatar avatar avatar-40 um-avatar um-avatar-uploaded" width="40" height="40" alt=""><a href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn"><?= get_the_author(); ?></a>';
				jQuery('.um-message-header-left').html(headerstring);
			}
		<?php } else { ?>
			var objectID = jQuery('#objectID').data('object_id'),
				conID = getParameterByName('conversation_id');
			jQuery('#dealbutton [data-object_id=' + objectID + ']').addClass('show');
			if ( jQuery('#dealbutton [data-conversation_id=' + conID + ']').length ) {
			} else {	
				jQuery('#dealbutton [data-object_id=' + objectID + ']:first').trigger( 'click' ).addClass('seeesome');
			}
		<?php } ?>
		jQuery('#openmessage').click(function(){
			jQuery(this).addClass('leave');
			jQuery('#dealbutton').removeClass('no-messages');
		});
	});
// 	jQuery(window).load(function(){
// 		
// 		if ( document.referrer.indexOf(location.protocol + "//" + location.host) === 0 ) {
// 			jQuery('#back').fadeIn();
// 			// console.log(document.referrer);	
// 		} else {
// 			// jQuery('#goToGallery').fadeIn();
// 		}	
// 		
// 	});
	</script>    

  </article>
<?php endwhile; ?>
