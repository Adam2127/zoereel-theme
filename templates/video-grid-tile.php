<?php // TODO: this was copied from content.php - repalce all instances of get_template_part of content with video-tile
	$file = get_field('video');
	$permission = get_field('viewing_permissions');
	if( $file ): ?>
<!-- templates/video-tile -->
<article class="tile <?= $permission ?>">
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
	<div class="tile__media">
		<?php echo wp_get_attachment_image( get_field('cover_art'), 'poster-sm', false, 'class=tile__imgOFF alignleft' ); ?>
	</div>
	<div class="tile__details">
		<div class="tile__desc">
			<?php 
				// $long_text = get_field('video_description' );
				// $max_length = 40; // we want to show only 40 characters.
				// 
				// if (strlen($long_text) > $max_length)
				// {
				// 	$short_text = (substr($long_text,0,$max_length-1)); // make it $max_length chars long
				// 	$short_text .= "..."; // add an ellipses ... at the end
				// 	// $short_text .= "<a href='#'>Read more</a>"; // add a link
				// 	echo '<p>' . $short_text . '</p>';
				// } else {
				// 	// string is already less than $max_length, so display the string as is
				// 	echo '<p>' . $long_text . '</p>';
				// }
			?>
  		</div>
	</div>	
	</a>
<?php get_template_part('partials/video', 'metrics'); ?>	
</article>

<?php endif; ?>