<?php
	$file = get_field('video');
	if( $file ): ?>
<!-- templates/content -->
<article class="tile">
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
	<div class="tile__media">
<!-- 		<video class="tile__img" src="<?php // echo $file['url']; ?>" poster="<?php // the_post_thumbnail_url(); ?>"><?php // echo $file['filename']; ?></video> -->
			<img class="tile__img" src="<?php the_post_thumbnail_url('poster-sm'); ?>">
	</div>
	<div class="tile__details">
		<div class="tile__title">
			<?php // the_title(); ?>
  		</div>
	</div>	
	</a>
<?php get_template_part('partials/video', 'metrics'); ?>	
</article>

<?php endif; ?>