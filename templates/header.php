<header id="header" class="banner">
  <div class="container">
		<a class="brand" href="<?= esc_url(home_url('/')); ?>"><img src="/wp-content/themes/zoereel/dist/images/color-white-text.svg" alt="<?php bloginfo('name'); ?>" /><span>Beta</span></a>
		<h3 class="outlinetext">Where New Ideas In Film & TV Get Made</h3>
    <nav class="nav-primary">
      <?php
      // https://www.w3schools.com/bootstrap/bootstrap_dropdowns.asp
      // https://github.com/wp-bootstrap/wp-bootstrap-navwalker
      wp_nav_menu( array(
      	'theme_location'    => 'browsed',
      	'depth'             => 2,
      	'menu_class'        => 'nav',
      	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
      	'walker'            => new WP_Bootstrap_Navwalker())
      );
      ?>	  
	  <?php get_search_form(); ?> 
	  <?php um_notification_show_feed(); ?>		   
      <?php
      if (has_nav_menu('primary_navigation')) :
        // wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif;
      
      // https://www.w3schools.com/bootstrap/bootstrap_dropdowns.asp
      // https://github.com/wp-bootstrap/wp-bootstrap-navwalker
      wp_nav_menu( array(
      	'theme_location'    => 'account',
      	'depth'             => 2,
      	'menu_class'        => 'nav',
      	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
      	'walker'            => new WP_Bootstrap_Navwalker())
      );
      ?>
    </nav>
  </div>
</header>