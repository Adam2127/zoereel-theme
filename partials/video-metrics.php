<!-- partials/video-metrics -->
<section class="metrics">
	<?php $vidID = isset($item->object_id) ? $item->object_id : $post->ID; ?>
	<?php echo do_shortcode('[post-views id="' . $vidID . '"]'); ?> 	
	<?php 
		// echo do_shortcode( '[likebtn theme="custom" btn_size="22" f_size="16" icon_size="22" icon_l_c_v="#e0c264" icon_d_c_v="#e0c264" counter_l_c="#ffffff" bg_c="rgba(250,250,250,0)" brdr_c="rgba(198,198,198,0)" like_enabled="0" dislike_enabled="0" show_like_label="0" icon_dislike_show="0" white_label="1" tooltip_enabled="0" popup_disabled="1" rich_snippet="1" share_enabled="0"  i18n_popup_text=""]' ); 
		echo do_shortcode('[likebtn theme="custom" btn_size="22" f_size="16" icon_size="22" icon_l_c_v="#e0c264" icon_d_c_v="#e0c264" counter_l_c="#ffffff" bg_c="rgba(250,250,250,0)" brdr_c="rgba(198,198,198,0)" like_enabled="0" dislike_enabled="0" show_like_label="0" icon_dislike_show="0" tooltip_enabled="0" white_label="1" rich_snippet="1" voter_by="user" user_logged_in="modal" user_logged_in_alert="<p class=&quot;alert-info fade in&quot; role=&quot;alert&quot;>You need to <a href=&quot;%url_login%&quot;>Login</a> or <a href=&quot;https://zoereel.com/sign-up/register/&quot;>Sign Up</a>in order to vote</p>" popup_disabled="1" popup_style="dark" share_enabled="0"]');
	?>
	<a class="count-comments" href="<?php the_permalink(); ?>#comment"><?php echo get_comments_number($vidID); // comments_number( '0', '1', '%' ); ?></a>
</section>