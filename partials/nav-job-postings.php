<?php $activepage = $post->post_name; $activetype =  $post->post_type; $userPost = ( ($post->post_author == get_current_user_id() ) ? true : false ); // echo $activetype; ?>
<?php // TODO: need to fix ?edit and active mine for postings that are authored by the current user ?>

<div class="um-profile-nav head jobs">
	<div class="um-profile-nav-item <?php echo ( $activepage == 'postings' || ($activetype == 'posting' && $userPost == false)  ) ? 'active' : '' ?>">
		<a href="/postings/" title="Job Listings"><i class="um-faicon-bullhorn"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">Jobs</span></a>
	</div>
	<div class="um-profile-nav-item <?php echo ($activepage == 'new-job-posting') ? 'active' : '' ?>">
		<a href="/new-job-posting/" title="Post Jobs for a Production"><i class="um-faicon-pushpin"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">Post a Job</span></a>
	</div>
	<div class="um-profile-nav-item <?php echo ($activepage == 'my-submissions' || $activetype == 'submission' ) ? 'active' : '' ?>">
		<a href="/my-submissions/" title="My Job Submissions"><i class="um-faicon-list"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">My Submissions</span></a>
	</div>
	<div class="um-profile-nav-item <?php echo ( $activepage == 'mine' || ($activetype == 'posting' && $userPost == true)  ) ? 'active' : '' ?>">
		<a href="/postings/mine/" title="My Job Posting"><i class="um-faicon-external-link"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">My Postings / Submissions</span></a>
	</div>
	<div class="um-clear"></div>
</div>

<script>

  document.addEventListener("DOMContentLoaded", function(event) {
	var elmnt = document.getElementsByClassName("active");
	elmnt[0].scrollIntoView(false, {behavior: "instant", block: "start", inline: "start"});
  });

</script>	