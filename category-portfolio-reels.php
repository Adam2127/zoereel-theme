<?php // get_template_part('templates/page', 'header'); ?>

<?php $category = get_queried_object(); ?>

<article class="scroll__rows">
	
<?php 
$postsperrow = 20;
$demoreelsubIDs = get_field('demo_reel_sub', 'option');

if( $demoreelsubIDs ): 

	foreach ($demoreelsubIDs as &$demoreelsubID) {
		
		$demoreelsub = get_term( $demoreelsubID, 'reel_categories' );
?>
	<h4><?php echo $category->name . ' - ' . $demoreelsub->name ?></h4>
	<section class="row">
		<span class="handle handlePrev" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-left"></span></span>
		<div class="row__inner">
<?php
$args= array(
	'post_type' => 'post',
	'numberposts'	=> $postsperrow,
	'cat'       => $category->term_id,
	'tax_query' => array(
		array(
			'taxonomy' => 'reel_categories',
			'field'    => 'ID',
			'terms'    => $demoreelsubID,
		),
	),
	'meta_key' => 'for_sale',
	'meta_value'	=> false,
	'meta_compare'  => '!=',
);
// 		$args = array(
// 			'numberposts'	=> $postsperrow,
// 			'cat'           => $category,
// 			'post_type'		=> 'post',
// 			'meta_key'		=> 'for_sale',
// 			'meta_value'	=> true,
// 			'meta_compare'  => '!=',
// 		);
		$videos = new WP_Query($args);
		while($videos->have_posts()) : $videos->the_post();
			get_template_part('templates'.$isMobile.'/video-tile', get_post_type() != 'post' ? get_post_type() : get_post_format());
		endwhile;
		wp_reset_postdata();
?>
	    </div>
	    <span class="handle handleNext show" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-right"></span></span>
	</section>
<?php
	}

endif;

?>

</article>


<?php // the_posts_navigation(); ?>
