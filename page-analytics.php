<?php // most viewed, most liked, most commented, most reeltalked, most surveyed ?>

<?php $mostarray = array('most-viewed', 'most-liked', 'most-commented', 'most-reeltalked', 'most-surveyed'); $most = $_SERVER['QUERY_STRING']; $most = (in_array($most, $mostarray) ? $most : 'most-viewed'); // echo $most; ?>


<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>  

<?php wp_reset_postdata(); ?>
    
  	<div class="um-profile-nav head analytics">
  	    <div class="um-profile-nav-item first <?php echo ($most == 'most-viewed') ? 'active' : '' ?>">
  	        <a href="?most-viewed" title="Audience Most Viewed"><i class="um-faicon-eye"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">Most Viewed</span></a>
  	    </div>
  	    <div class="um-profile-nav-item <?php echo ($most == 'most-liked') ? 'active' : '' ?>">
  	        <a href="?most-liked" title="Audience Most Liked"><i class="um-faicon-star"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">Most Liked</span></a>
  	    </div>
  	    <div class="um-profile-nav-item <?php echo ($most == 'most-commented') ? 'active' : '' ?>">
  	        <a href="?most-commented" title="Audience Most Commented"><i class="um-faicon-comment"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">Most Commented</span></a>
  	    </div>
  	    <div class="um-profile-nav-item <?php echo ($most == 'most-reeltalked') ? 'active' : '' ?>">
  	        <a href="?most-reeltalked" title="Filmmakers Most Reeltalked"><i class="um-faicon-comments-o"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">Most Reeltalked</span></a>
  	    </div>
  	    <div class="um-profile-nav-item <?php echo ($most == 'most-surveyed') ? 'active' : '' ?>">
  	        <a href="?most-surveyed" title="Audience Most Surveyed"><i class="um-faicon-list"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">Most Surveyed</span></a>
  	    </div>
  	    <div class="um-clear"></div>
  	</div>
  	<div class="um-profile-body posts posts-default video-grid">

<?php	
$postsperpage = 20;
$showmetrics = true;

switch ($most) {
    case "most-liked":
    	echo do_shortcode('[likebtn_most_liked title="" number="'.$postsperpage.'" order="likes" time_range="all" show_likes="1" show_dislikes="0" show_thumbnail="1" thumbnail_size="thumbnail" show_excerpt="1" show_date="0" entity_name="post"]');
// 			  $args = array(
// 			   'posts_per_page' => $postsperpage,
// 			   'post_type'  => 'post',
// 			   'meta_key'   => 'wpfp_favorites',
// 			   'orderby'    => 'meta_value_num',
// 			  );
// 			  $query = new WP_Query( $args );
// 			  if ( $query->have_posts() ) {
// 			   while ( $query->have_posts() ) {
// 			   	$query->the_post();
// 			   		include( locate_template( 'templates/video-loop.php', false, false ) );
// 			   }
// 			  }        
        break;


    case "most-commented":
    		  $getcomments = true;	
    		  $commenttype = 'comment';	

			  $args = array(
				'post_type'              => 'post',
			  	'posts_per_page'         => $postsperpage,
			  	'ignore_sticky_posts'    => 1,
			  	'orderby'                => 'comment_count'
			  );

add_filter( 'posts_where', 'filter_w_comments' );
$query = new WP_Query( $args );
remove_filter( 'posts_where', 'filter_w_comments' );
			  if ( $query->have_posts() ) {
			   while ( $query->have_posts() ) {
			   	$query->the_post();
			   	// get_template_part('templates'.$isMobile.'/content', get_post_type() != 'post' ? get_post_type() : get_post_format());
			   		include( locate_template( 'templates/video-loop.php', false, false ) );
			   }
			  }
        break;


    case "most-reeltalked":
// TODO: fix this query https://www.advancedcustomfields.com/resources/orde-posts-by-custom-fields/
			  $getcomments = true;
			  $commenttype = 'reeltalk';
			  
			  $videoIDs = $wpdb->get_results("SELECT $wpdb->posts.ID FROM $wpdb->posts  WHERE $wpdb->posts.reeltalk_count > 0  AND $wpdb->posts.post_type = 'post' AND ($wpdb->posts.post_status = 'publish' OR $wpdb->posts.post_status = 'private')  ORDER BY $wpdb->posts.reeltalk_count DESC");	

			  if ( $videoIDs ) 
			  {
			  	foreach ( $videoIDs as $videoID ) 
			  	{ 
			  		$post = get_post( intval( $videoID->ID ) );
			  		setup_postdata( $post );
			  		include( locate_template( 'templates/video-loop.php', false, false ) );	
			  	} 
			  }
        break;


    case "most-surveyed":
    		  $getsurveys = true;	

			  global $wpdb;
			  
			  $videoIDs = $wpdb->get_results("SELECT $wpdb->posts.ID FROM $wpdb->posts  WHERE $wpdb->posts.surveys_count > 0  AND $wpdb->posts.post_type = 'post' AND ($wpdb->posts.post_status = 'publish' OR $wpdb->posts.post_status = 'private')  ORDER BY $wpdb->posts.surveys_count DESC
			  
			  ");
			  
			  if ( $videoIDs ) 
			  {
			  	foreach ( $videoIDs as $videoID ) 
			  	{ 
			  		$post = get_post( intval( $videoID->ID ) );
			  		setup_postdata( $post );
			  		include( locate_template( 'templates/video-loop.php', false, false ) );	
			  	} 
			  }
        break;        


    default:
	  		  $posts = pvc_get_most_viewed_posts(
	  		  		array(
	  		  			'posts_per_page' => $postsperpage,
	  		  			'order'			 => 'desc',
	  		  			'post_type'		 => 'post'
	  		  		)
	  		  	);
			  foreach ( $posts as $post ) {
			  	setup_postdata( $post );
			  		include( locate_template( 'templates/video-loop.php', false, false ) );
			  	}
}
wp_reset_postdata();
?>  
 
  	</div> 

<script>

  document.addEventListener("DOMContentLoaded", function(event) {
	var elmnt = document.getElementsByClassName("active");
	if (!elmnt[1].classList.contains('first')) {
    	elmnt[1].scrollIntoView(false, {behavior: "instant", block: "center", inline: "nearest"} );
    }
  });

</script>
 
<?php
// Show user Bookmark/Favorites
// echo do_shortcode('[cbxwpbookmark userid="'.um_user("ID").'"]');
?>  	