<?php if (is_super_admin()) { echo '<!--  ' . basename(__FILE__) . ' -->'; } ?>
<?php $access = ( is_user_logged_in() && in_array( um_user('role'), array('studio','admin')  ) ) ? 'studio' : 'artist'; ?>
<?php $salearray = array('for-sale', 'mine-for-sale', 'upload-for-sale'); $sale = $_SERVER['QUERY_STRING']; $sale = (in_array($sale, $salearray) ? $sale : 'for-sale'); // echo $sale; ?>
<?php 
	if( $access == 'artist' ) { $sale = 'mine-for-sale'; }
?>
<div class="um-profile-nav">
	<?php 
		if( $access == 'studio' ) {
	?>
	<div class="um-profile-nav-item um-profile-nav-posts <?php echo ($sale == 'for-sale') ? 'active' : '' ?>">
		<a href="?for-sale" title="Videos"><i class="um-faicon-film"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">Movies For Sale</span></a>
	</div>
	<?php } ?>
	<div class="um-profile-nav-item um-profile-nav-posts <?php echo ($sale == 'mine-for-sale') ? 'active' : '' ?>">
		<a href="?mine-for-sale" title="Videos"><i class="um-faicon-film"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">My Movies For Sale</span></a>
	</div>	
	<div class="um-profile-nav-item um-profile-nav-main">
		<a href="/new-upload-for-sale/" title="Upload New Trailer"><i class="um-faicon-cloud-upload"></i> <span class="uimob500-hide uimob340-hide uimob800-hide title">Sell Your Movie</span></a>
	</div>
	<div class="um-clear"></div>
</div>
		
<?php while (have_posts()) : the_post(); ?>
  <?php // get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<?php 
	$mine = ($sale == 'mine-for-sale') ? $current_user->ID : '-0';
// https://www.advancedcustomfields.com/resources/query-posts-custom-fields/
$args = array(
	'numberposts'	=> 5,
	'post_type'		=> 'post',
	'meta_key'		=> 'for_sale',
	'meta_value'	=> true,
	'ignore_sticky_posts' => true,
	'author'        => $mine
);


// query
$the_query = new WP_Query( $args );
$zoeUrl = get_home_url();
$saleBase = '/movie-for-sale/';
$queryVar = '/?forsale';
?>
<?php if( $the_query->have_posts() ): ?>
	<section>
	<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<?php include( locate_template( 'templates/video-loop.php', false, false ) ); ?>
	<?php endwhile; ?>
	</section>
<?php endif; ?>

<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>

<script>

  document.addEventListener("DOMContentLoaded", function(event) {
	var elmnt = document.getElementsByClassName("active");
	if (!elmnt[1].classList.contains('um-profile-nav-posts')) {
    	elmnt[1].scrollIntoView(false, {behavior: "instant", block: "center", inline: "nearest"});
    }
  });

</script>	