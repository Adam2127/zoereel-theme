<?php
	$userPost = ( ($post->post_author == get_current_user_id() ) ? true : false );

	if (!$userPost || (strpos($_SERVER['QUERY_STRING'], 'edit') === 0 && $userPost)) { acf_form_head(); }
?>

<?php get_template_part('partials/nav', 'job-postings'); ?>

<?php 
	if ( strpos($_SERVER['QUERY_STRING'], 'edit') === 0 && $userPost ) {

		get_template_part('templates/content-posting-edit', get_post_type());

	} else {

		get_template_part('templates/content-posting', get_post_type());

		if (!$userPost) { acf_form('submission'); }
		else {
	
		    $submissions = get_posts(array(
				'post_type' => array( 'submission' ), // uncomment it if you only want to get the result from movies post type
				'posts_per_page' => -1,
				'meta_query' => array(
					array(
						'key' => 'job_posting',
						'value' => '"' . get_the_ID() . '"',
						'compare' => 'LIKE'
					)
				)
			));
			?>
		
			<article id="submissions">
			  <header>
			    <h2>(<?php echo count($submissions); ?>) Submissions for <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
			  </header>
			  <br>
			  <br>
			  <div class="entry-summary">
				  <table id="survey-table">
			<?php
			if ( $submissions )
			{
				foreach ( $submissions as $post )
				{
					setup_postdata( $post );
			?>
				    		<tr>
				    			<td><a href="<?= um_user_profile_url(); ?>" rel="author" class="fn"><?= get_the_author(); ?></a></td>
			
				    			<td><a href="<?php the_permalink(); ?>">View Submission</a></td>
				    		</tr>
				    		<?php
				}
			}
			wp_reset_postdata();
			?>
				  </table>
			  </div>
			</article>
	
	<?php } ?>

	<script>
		document.getElementById("acf-_post_title").value = 'Submission: <?php the_title(); ?>';
	</script>

<?php } ?>	