<?php get_template_part('partials/nav', 'job-postings'); ?>

<?php acf_form_head(); ?>
<?php if (is_super_admin()) { echo '<!--  ' . basename(__FILE__) . ' -->'; } ?>

<?php while (have_posts()) : the_post(); ?>
  <?php // get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

	<?php acf_form('job_posting'); ?>