<?php if($isMobile != '-mobile') { ?>
<div class="alignleft" data-dimensions="160x600" style="position: absolute;top: 0;left:-175px;">
	<?php if( function_exists('the_ad_placement') ) { the_ad_placement('video-ads'); } ?>
</div>
<?php } ?> 

<?php  
	// $sale = get_query_var('forsale');
	// if ($sale) { 
	$showMessages = ( $post->post_author == get_current_user_id() ? ( get_field('for_sale') == true ? true : false ) : false );
	$sale = $_SERVER['QUERY_STRING'];
	// if ($sale == 'forsale') {
	if ( ( strpos($sale, 'forsale') !== false || strpos($sale, 'message') !== false || $showMessages == true ) && strpos($sale, 'surveys') === false ) {
?>

<?php get_template_part('templates/content-forsale', get_post_type()); ?>		

<?php } elseif (strpos($sale, 'surveys') !== false )  { ?>
  	
<?php get_template_part('templates/content-surveys', get_post_type()); ?>

<?php } else { ?>
  	
<?php get_template_part('templates/content-single', get_post_type()); ?>

<?php } ?>

<?php if($isMobile != '-mobile') { ?>
<div class="alignright" data-dimensions="160x600" style="position: absolute;top: 0;right:-175px;">
    <?php if( function_exists('the_ad_placement') ) { the_ad_placement('video-ads'); } ?>
</div>
<?php } ?> 


<script>
document.addEventListener("DOMContentLoaded", function(){
// https://github.com/mediaelement/mediaelement/blob/master/docs/api.md	
	var isPlaying = false, duration = progress = 0;
	
var player = new MediaElementPlayer( 'video-<?php echo $post->ID;?>', {
	stretching : "responsive",
	// classPrefix : "zoe-",
	pluginPath : "\/wp-includes\/js\/mediaelement\/",
	success: function( mediaElement ) { 
		mediaElement.addEventListener('loadedmetadata', function(){
			duration = mediaElement.duration;
		    console.log(mediaElement.duration);    
		});
		mediaElement.addEventListener( 'playing', function( e ) {
			isPlaying = true; //keep track of if it’s playing or not
			document.body.classList.add('playing');
			console.log('playing');
		}, false);
		
		// Event listener for when the video is paused
		mediaElement.addEventListener( 'pause', function( e ) {
			isPlaying = false;
			progress = player.getCurrentTime() / duration;
			document.body.classList.remove('playing');
			console.log( player.getCurrentTime() );
			console.log('paused');
			console.log('progress ' + progress );
		}, false);
		
		// Event listener for when the video ends
		mediaElement.addEventListener( 'ended', function( e ) {
			isPlaying = false;
			progress = 100;
			document.body.classList.remove('playing');
			console.log('ended');
		}, false);
		
	}
} );
	
} );
</script>
