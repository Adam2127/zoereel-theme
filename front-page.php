<?php get_template_part('templates/page', 'header'); ?>

<?php $featuredID = get_field('featured_video', 'option'); $postsperrow = 20; $viewmore = get_field('view_all_cat', 'option'); ?>

<section id="featured">
	<?php 
		$query = new WP_Query( array( 'p' => $featuredID ) );
		while($query->have_posts()) : $query->the_post();
			get_template_part('templates'.$isMobile.'/content-large', get_post_type() != 'post' ? get_post_type() : get_post_format());
		endwhile;
		wp_reset_postdata();
	?>
</section>

<article class="scroll__rows">
	<h4>Zoereel Picks</h4>
	<section class="row">
		<span class="handle handlePrev" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-left"></span></span>
		<div class="row__inner">
			<?php
			$argstop = array(
				'numberposts'	=> $postsperrow,
				'cat'           => '-100', // exclude Demo Reels
				'post_type'		=> 'post',
				'meta_key'		=> 'for_sale',
				'meta_value'	=> false,
				'meta_compare'  => '!=',
			);	

			$toprow= new WP_Query($argstop);
			while($toprow->have_posts()) : $toprow->the_post();
				get_template_part('templates'.$isMobile.'/video-tile', get_post_type() != 'post' ? get_post_type() : get_post_format());
			endwhile;
			wp_reset_postdata();
			?>
	    </div>
		<span class="handle handleNext show" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-right"></span></span>	    
	</section>
	
	<h4>Trending</h4>        
	<section class="row">
		<span class="handle handlePrev" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-left"></span></span>
		<div class="row__inner">
			<?php
	  		  $posts = pvc_get_most_viewed_posts(
	  		  		array(
	  		  			'posts_per_page' => $postsperrow,
	  		  			'cat'           => '-100', // exclude Demo Reels
	  		  			'order'			 => 'desc',
	  		  			'post_type'		 => 'post',
	  		  			'meta_key'		=> 'for_sale',
	  		  			'meta_value'	=> true,
	  		  			'meta_compare'  => '!=',	  		  			
	  		  		)
	  		  	);
			  foreach ( $posts as $post ) {
			  	setup_postdata( $post );
			  		get_template_part('templates'.$isMobile.'/video-tile', get_post_type() != 'post' ? get_post_type() : get_post_format());
			  	}
			  wp_reset_postdata();				
	        // while ( have_posts() ) : the_post();
			// 	get_template_part('templates'.$isMobile.'/video-tile', get_post_type() != 'post' ? get_post_type() : get_post_format());
	        // endwhile;
	        // wp_reset_query();
	        ?>
	    </div>
	    <span class="handle handleNext show" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-right"></span></span>
	</section>


<?php 
$categoryIDs = get_field('homepage_categories', 'option');

if( $categoryIDs ): 

	foreach ($categoryIDs as &$categoryID) {
?>
	<h4><?php echo get_cat_name($categoryID); ?></h4> 
	<?php if (in_array($categoryID, $viewmore)) { 
		echo '<a class="view" href="' . get_category_link($categoryID) . '"> View All</a>';
	} else {
		echo '<span class="parenthetical">( waiting for your new ideas )</span>';
	} ?>
	<section class="row">
		<span class="handle handlePrev" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-left"></span></span>
		<div class="row__inner">
<?php
		$args = array(
			'numberposts'	=> $postsperrow,
			'cat'           => $categoryID,
			'post_type'		=> 'post',
			'meta_key'		=> 'for_sale',
			'meta_value'	=> true,
			'meta_compare'  => '!=',
		);
		$videos = new WP_Query($args);
		while($videos->have_posts()) : $videos->the_post();
			get_template_part('templates'.$isMobile.'/video-tile', get_post_type() != 'post' ? get_post_type() : get_post_format());
		endwhile;
		wp_reset_postdata();
?>
	    </div>
	    <span class="handle handleNext show" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-right"></span></span>
	</section>
<?php
	}

endif;

?>

	<?php
		// $favorite_post_ids = wpfp_get_user_meta();
	    // if ($favorite_post_ids) {
		// 	$favorite_post_ids = array_reverse($favorite_post_ids);
	    //     $post_per_page = wpfp_get_option("post_per_page");
	    //     $page = intval(get_query_var('paged'));
		// 
	    //     $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
	    //     // custom post type support can easily be added with a line of code like below.
	    //     // $qry['post_type'] = array('post','page');
	    //     query_posts($qry);
	?>
<!--
	<h4>Your Favorites</h4>        
	<section class="row">
		<span class="handle handlePrev" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-left"></span></span>
		<div class="row__inner">
			<?php
	        // while ( have_posts() ) : the_post();
			// 	get_template_part('templates'.$isMobile.'/video-tile', get_post_type() != 'post' ? get_post_type() : get_post_format());
	        // endwhile;
	        // wp_reset_query();
	        ?>
	    </div>
	    <span class="handle handleNext show" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-right"></span></span>
	</section>
-->
	<?php    
	    // }
	?>
	
<?php 
	if( is_user_logged_in() && in_array( um_user('role'), array('artist','studio','admin')  ) ) {
?>
	<h4>Portfolio Reels</h4> <a class="view" href="/portfolio-reels/"> View All</a>
	<section class="row">
		<span class="handle handlePrev" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-left"></span></span>
		<div class="row__inner">
			<?php 	
			$demoreeels = new WP_Query('cat=100'); // exclude category
			while($demoreeels->have_posts()) : $demoreeels->the_post();
				get_template_part('templates'.$isMobile.'/video-tile', get_post_type() != 'post' ? get_post_type() : get_post_format());
			endwhile;
			wp_reset_postdata();
			?>
	    </div>
	    <span class="handle handleNext show" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-right"></span></span>
	</section>
<?php
	}
?>		
	
</article>
