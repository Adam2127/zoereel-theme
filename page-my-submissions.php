<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<?php acf_form_head(); ?>
<?php
	$posterID = get_current_user_id();
?>

<?php get_template_part('partials/nav', 'job-postings'); ?>

<section>
	
	<table>
		<tr>
			<th>Date Submitted</th> <th>Project Title</th> <th>Job</th> <th>Casting Director</th>
		</tr>	
			<?php 
				$args = array(
					'post_type' => 'submission',
					'posts_per_page'      => 1000,
					'author' => $posterID,
				);
	
	// https://codex.wordpress.org/Function_Reference/the_date
	
	$query = new WP_Query( $args );
			while($query->have_posts()) : $query->the_post();
			
				echo '<tr>';
				
					echo '<td>'.get_the_date().'</td>';
					// echo '<td><a href="'.get_permalink().'">'.get_the_title().'</a></td>';
		$postings = get_field('job_posting');
					echo '<td><a href="'.get_permalink( $postings[0] ).'">'.get_the_title( $postings[0] ).'</a></td>';
					echo '<td>'.get_field('job_applying_for').'</td>';
					echo '<td>'.get_field('submission_comment').'</td>';
				
				echo '</tr>';   
			
			endwhile;
			
			wp_reset_postdata();
			?>
	</table>

</section>


		<?php // Loop 2
//		$toprow= new WP_Query('cat=-17'); // exclude Demo Reels
// 		while($toprow->have_posts()) : $toprow->the_post();
// 			get_template_part('templates'.$isMobile.'/content', get_post_type() != 'post' ? get_post_type() : get_post_format());
// 		endwhile;
// 		wp_reset_postdata();
		?>